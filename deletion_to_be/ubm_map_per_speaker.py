__author__ = 'Maciej Aleszczyk'

'''
UBM MAP per speaker:

input: directory containing files with MFCC parameters from enrollment set files are in format .hdf5
       path to UBM-GMM
output: hdf5 files, each containing GMM-MAP approximation specific for each speaker

Enroll processed set path: /home/ragnar/speaker_data/enroll/processed/
GMM-UBM path: /home/ragnar/speaker_data/ubm_train/
'''

import glob
import time

import bob.learn.misc

from feature_extraction import mfcc_extraction
from deletion_to_be import gmm_extraction


def get_file_list(path):
    """
    Function used to get hdf5 files from specific path
    :param path: path to directory with MFCC files
    :return: :rtype: list of files
    """
    file_list = glob.glob(path + "*.hdf5")
    return file_list


def run(files_path, ubm_path):
    """
    Function uses MAP-GMM to create model for each speaker. Created models are saved in adapted directory in input path.
    :param files_path: path to hdf5 files with MFCC parameters to adapt UBM to specific speaker
    :param ubm_path: path to UBM
    """
    file_list = get_file_list(files_path)
    ubm_file = get_file_list(ubm_path)[0]

    ubm = gmm_extraction.open_gmm(ubm_file)

    relevance_factor = 4.
    trainer = bob.learn.misc.MAP_GMMTrainer(relevance_factor, True, False, False)  # mean adaptation only
    trainer.convergence_threshold = 1e-5
    trainer.max_iterations = 200
    trainer.set_prior_gmm(ubm)

    start = time.time()

    for i in range(0, len(file_list)):
        print
        print "             *******             "
        print "Processing speaker " + file_list[i][-9:-5] + " MAP-GMM adaptation"
        print "             *******             "
        print
        features = mfcc_extraction.open_features_file(file_list[i])

        gmm_adapted = bob.learn.misc.GMMMachine(64, 57)
        trainer.train(gmm_adapted, features)

        gmm_extraction.save_gmm(ubm_path + 'adaptated/ubm_adapted_64_' + file_list[i][-9:-5] + '.hdf5', gmm_adapted)

    stop = time.time()
    print "total time " + str(stop - start)


# run('/home/ragnar/speaker_data/enroll/processed/', '/home/ragnar/speaker_data/ubm_train/')
