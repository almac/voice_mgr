#
# import train_speakers
# import energy_vad
# import scipy
# import bob.ap
# import numpy as np
# from numpy.fft import fft, ifft
# from scipy.signal import lfilter, hamming
# from scikits.talkbox import lpc
# import math
#
# pathSpeaker = '/home/ragnar/speaker_data/enroll/'
# files = train_speakers.get_file_list(pathSpeaker)
#
#
# file = files['f118'][4]
# # print files
# # for file in files:
# #     print file
# #     rate, signal = scipy.io.wavfile.read(file)
# #     print len(signal)
# #     fftResult=np.log(abs(fft(signal)))
# #     # ceps=ifft(fftResult);
# #     #
# #     # ceps_real = abs(ceps)
# #
# #     print fftResult.argsort()[-5:][::-1]*rate/len(signal)
# #     del signal, fftResult
#
# def glottal(file):
# rate, signal = scipy.io.wavfile.read(file)
# features = energy_vad.energy_vad(file)
# win_length_ms = 30  # The window length of the cepstral analysis in milliseconds
#     win_shift_ms = 10  # The window shift of the cepstral analysis in milliseconds
#     n_filters = 22  # The number of filter bands
#     n_ceps = 19  # The number of cepstral coefficients
#     f_min = 0.  # The minimal frequency of the filter bank
#     f_max = 8000.  # The maximal frequency of the filter bank
#     delta_win = 2  # The integer delta value used for computing the first and second order derivatives
#     pre_emphasis_coef = 0.97  # The coefficient used for the pre-emphasis
#     dct_norm = True  # A factor by which the cepstral coefficients are multiplied
#     mel_scale = True  # Tell whether cepstral features are extracted on a linear (LFCC) or Mel (MFCC) scale
#     with_delta = False  # If MFCC deltas are computed
#     with_delta_delta = False  # If MFCC deltas are computed
#     with_energy = True  # if another energy feature MFCC[0]
#     vad_type = 0  # if 0 energy vad if 1 stat vad
#     mfcc_parameters = {'win_length_ms': win_length_ms, 'win_shift_ms': win_shift_ms, 'n_filters': n_filters,
#                        'n_ceps': n_ceps, 'f_min': f_min, 'f_max': f_max, 'delta_win': delta_win,
#                        'pre_emphasis_coef': pre_emphasis_coef, 'dct_norm': dct_norm, 'mel_scale': mel_scale,
#                        'with_delta': with_delta, 'with_delta_delta': with_delta_delta, 'with_energy': with_energy,
#                        'vad_type': vad_type}
#     c = bob.ap.Ceps(16000, mfcc_parameters['win_length_ms'], mfcc_parameters['win_shift_ms'],
#                     mfcc_parameters['n_filters'], mfcc_parameters['n_ceps'], mfcc_parameters['f_min'],
#                     mfcc_parameters['f_max'], mfcc_parameters['delta_win'], mfcc_parameters['pre_emphasis_coef'],
#                     mfcc_parameters['mel_scale'], mfcc_parameters['dct_norm'])
#     c.with_delta = mfcc_parameters['with_delta']
#     c.with_delta_delta = mfcc_parameters['with_delta_delta']
#     c.with_energy = mfcc_parameters['with_energy']
#     mfcc = c(features['samples'])
#     print len(mfcc)
#     # in ms
#     frame = 30
#     step = 10
#     rate = 16000
#     n = frame * rate / 1000
#     step2 = step * rate / 1000
#     # print len(features['samples'])
#     # print "****************"
#     # b = np.array([features['samples'][i:i + n] for i in range(0, len(features['samples']) - n + 1, step2)])
#     # print len(b)
#     #
#     # for step in b:
#     #     # fftResult = np.log(abs(fft(step)));
#     #     # ceps = np.absolute(ifft(fftResult));
#     #     # posmax = ceps[1:].max();
#     #     # # nceps = ceps.shape[-1] * 2 / 3
#     #     # # peaks = []
#     #     # # k = 3
#     #     # # while (k < nceps - 1):
#     #     # #     y1 = (ceps[k - 1])
#     #     # #     y2 = (ceps[k])
#     #     # #     y3 = (ceps[k + 1])
#     #     # #     if (y2 > y1 and y2 >= y3): peaks.append([float(rate) / (k + 2), abs(y2), k, nceps])
#     #     # #     k = k + 1
#     #     # # maxi = max(peaks, key=lambda x: x[1])
#     #     # # print maxi[0]
#     #     # print posmax
#     #     N = len(step)
#     #     w = np.hamming(N)
#     #
#     #     # Apply window and high pass filter.
#     #     x1 = step * w
#     #     x1 = lfilter([1], [1., 0.63], x1)
#     #
#     #     # Get LPC.
#     #     ncoeff = 2 + rate / 1000
#     #     A, e, k = lpc(x1, ncoeff)
#     #
#     #     # Get roots.
#     #     rts = np.roots(A)
#     #     rts = [r for r in rts if np.imag(r) >= 0]
#     #
#     #     # Get angles.
#     #     angz = np.arctan2(np.imag(rts), np.real(rts))
#     #
#     #     # Get frequencies.
#     #     frqs = sorted(angz * (rate / (2 * math.pi)))
#     #     print frqs[:3]
#
#     results = []
#     #
#     # fftResult = np.log(abs(fft(signal)));
#     # ceps = np.absolute(ifft(fftResult));
#     # posmax = ceps[1:].max();
#     # nceps = ceps.shape[-1] * 2 / 3
#     # peaks = []
#     # k = 3
#     # while (k < nceps - 1):
#     #     y1 = (ceps[k - 1])
#     #     y2 = (ceps[k])
#     #     y3 = (ceps[k + 1])
#     #     if (y2 > y1 and y2 >= y3): peaks.append([float(rate) / (k + 2), abs(y2), k, nceps])
#     #     k = k + 1
#     # maxi = max(peaks, key=lambda x: x[1])
#     # results.extend(maxi)
#     N = len(features['samples'])
#     w = np.hamming(N)
#     # Apply window and high pass filter.
#     x1 = features['samples'] * w
#     x1 = lfilter([1], [1., 0.63], x1)
#     # Get LPC.
#     ncoeff = 2 + rate / 1000
#     A, e, k = lpc(x1, ncoeff)
#     # Get roots.
#     rts = np.roots(A)
#     rts = [r for r in rts if np.imag(r) >= 0.1]
#     # Get angles.
#     angz = np.arctan2(np.imag(rts), np.real(rts))
#     # Get frequencies.
#     frqs = sorted(angz * (rate / (2 * math.pi)))
#     results.extend(frqs[:3])
#     print results
#
# for file in files['f122']:
#     glottal(file)
# print "*********"
# for file in files['f118']:
#     glottal(file)

# a = [0, 1, 2, 3]

# print a[1:3]

# import glob
#
# file_list = glob.glob('/home/ragnar/crossvalidation/1/enroll/' + "*." + 'wav')
# print len(file_list)
