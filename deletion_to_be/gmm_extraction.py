__author__ = 'Maciej Aleszczyk'

import os
import time

import bob.io.base


def save_gmm(path, gmm):
    """
    Function saves GMM in specific location as hdf5 file
    :param path: path to save GMM
    :param gmm: Gaussian Mixture Model to be saved
    """
    print "Saving to HDF5 File"
    start = time.time()

    if not os.path.isdir(path[0:path.rfind('/')]):
        os.makedirs(path[0:path.rfind('/')])

    f = bob.io.base.HDF5File(path, 'w')
    f.set('weights', gmm.weights)
    f.set('means', gmm.means)
    f.set('variances', gmm.variances)
    del f
    stop = time.time()
    print "Saved in " + str(stop - start) + " s"
    print "-----------------------------------"


def open_gmm(path):
    """
    Function opens hdf5 file with saved GMM and returns it
    :param path: path to GMM hdf5 file
    :return: :rtype: GMM bob object
    """
    print "Opening HDF5 File"
    start = time.time()
    gmm_file = bob.io.base.HDF5File(path)
    gmm = bob.learn.misc.GMMMachine(64, 57)
    gmm.weights = gmm_file.read('weights')
    gmm.means = gmm_file.read('means')
    gmm.variances = gmm_file.read('variances')

    stop = time.time()
    print "Opened and read in " + str(stop - start) + " s"
    return gmm

