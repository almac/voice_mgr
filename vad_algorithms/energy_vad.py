__author__ = 'Maciej Aleszczyk'
import numpy as np
import scipy.io.wavfile
import math
import itertools
import sys


def chunks(l, n):
    """
    Cut signal into n-length parts
    :param l: signal to be chunked
    :param n: chunk length
    :return: chunked signal
    """
    if n < 1:
        n = 1
    return [l[i:i + n] for i in range(0, len(l), n)]


def energy_from_signal(signal):
    """
    Computes energy from signal
    :param signal:  signal to be processed
    :return: energy from signal
    """
    return [sum(abs(signal[i]) ** 2) for i in range(0, len(signal))]


def energy_vad(wave_path, win_length_ms_vad=10):
    """
    Computes energy VAD from file
    :param wave_path: file path
    :param win_length_ms_vad: The windows length of the VAD analysis in milliseconds
    :return: processed signal
    """

    rate, signal = scipy.io.wavfile.read(str(wave_path))
    signal = np.cast['float'](signal)  # vector should be in **float**
    signal_framed = chunks(signal, win_length_ms_vad * rate / 1000)
    energy = energy_from_signal(signal_framed)

    energy_max = max(energy)
    energy_min = min(energy)

    if energy_min == 0:
        energy_min = np.spacing(1)  # min(n for n in energy if n != energy_min)

    threshold1 = energy_min * (1 + 2 * math.log10(energy_max / energy_min))

    # sum_energy = 0.0
    # count_energy = 0
    # for e in energy:
    # if e >= threshold1:
    # sum_energy += e
    # count_energy += 1
    #
    # sl = sum_energy/count_energy

    sl = np.mean(energy)

    threshold2 = threshold1 + 0.25 * (sl - threshold1)

    signal_framed = [v for i, v in enumerate(signal_framed) if not energy[i] <= threshold2]
    signal_framed = [v for i, v in enumerate(signal_framed) if not energy[i] < threshold1]

    last_sig = list(itertools.chain.from_iterable(signal_framed))
    last_sig = np.cast['float'](last_sig)
    sys.stdout.write('.')
    return {'samples': last_sig, 'file': wave_path.split('/')[-1][0:4]}



