__author__ = 'Maciej Aleszczyk'

import processing_core as core

# -----------------------------------------------------------------------------------------------------------------
# ------------PARAMETERS-------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------

# ------------PROCESSING TYPE--------------------------------------------------------------------------------------

processing_type = "GBRT"  # Processing types: RI- rules induction, RF- random forest, DT- decision tree, GMMUBM, GMM, GBRT

# ------------FILES------------------------------------------------------------------------------------------------

path_ubm_training = '/home/ragnar/speaker_data/ubm_train/female/'  # Path to training set
path_model_training = '/home/ragnar/speaker_data/enroll/'  # Path to model eval set
path_probe_data = '/home/ragnar/speaker_data/probe/'  # Path to probe set
path_results = '/home/ragnar/SpeakerRecognition/'  # Path to result catalog

# ------------MFCC-------------------------------------------------------------------------------------------------

win_length_ms = [25, 25]  # The window length of the cepstral analysis in milliseconds
win_shift_ms = [25, 25]  # The window shift of the cepstral analysis in milliseconds
n_filters = [80, 80]  # The number of filter bands
n_ceps = [32, 32]  # The number of cepstral coefficients
f_min = [0., 0.]  # The minimal frequency of the filter bank
f_max = [8000., 8000.]  # The maximal frequency of the filter bank
delta_win = [2, 2]  # The integer delta value used for computing the first and second order derivatives
pre_emphasis_coef = [0.97, 0.97]  # The coefficient used for the pre-emphasis
dct_norm = [True, True]  # A factor by which the cepstral coefficients are multiplied
mel_scale = [True, True]  # Tell whether cepstral features are extracted on a linear (LFCC) or Mel (MFCC) scale
with_delta = [True, True]  # If MFCC deltas are computed
with_delta_delta = [True, True]  # If MFCC deltas are computed
with_energy = [True, True]  # if another energy feature MFCC[0]
vad_type = [0, 0]  # if 0 energy vad if 1 stat vad

# ------------GMM--------------------------------------------------------------------------------------------------

relevance_factor = 8.
convergence_threshold = 1e-5
max_iterations = 200
gaussians_no = 64

# ------------TREE-------------------------------------------------------------------------------------------------

tree_type = "CART"  # Used tree type C45 or CART
criterion_type = "gini"  # Criterion in splitting: gini or entropy
forest_size = 5  # Random forest size

# ------------FUSION----------------------------------------------------------------------------------------------
fusion_type = 'fusion_scale'  # fusion_scale fusion_addition
fusion_tree = 'DT'  # RF DT

# ------------GRADIENT TREE ---------------------------------------------------------------------------------------

estimators = 10  # number of boosting stages
max_depth = 3   # maximum depth of tree
learning_rate = 0.1 # tree contribution


# -----------------------------------------------------------------------------------------------------------------
# ------------RECOGNITION SCRIPT-----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------

core.run(processing_type,
         path_ubm_training, path_model_training, path_probe_data, path_results,
         win_length_ms, win_shift_ms, n_filters, n_ceps, f_min, f_max, delta_win, pre_emphasis_coef,
         dct_norm,
         mel_scale, with_delta, with_delta_delta, with_energy, vad_type,
         relevance_factor, convergence_threshold, max_iterations, gaussians_no,
         tree_type, criterion_type, forest_size,
         fusion_type, fusion_tree,
         estimators, max_depth, learning_rate)
