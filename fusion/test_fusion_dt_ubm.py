from __future__ import division
import glob
from sklearn import tree
import datetime
import os
from sklearn import ensemble
import shutil

from feature_extraction import mfcc_extraction_dt
from misc import testing_utils as test
from misc import file_utils as futil
from gmm_ubm import train_speakers


def write_info_parameter_tag(path_results, mfcc_parameters, tree_parameters, mfcc_parameters_fusion, fusion_type,
                             fusion_tree):
    """
    Function creates info file containing parameters used in current processing

    :param path_results: path to result of processing, place where file is created
    :param mfcc_parameters: dictionary with mfcc parameters for tree algorithms
    :param tree_parameters: dictionary with decision tree parameters
    :param mfcc_parameters_fusion: dictionary with mfcc parameters for GMM algorithm
    :param fusion_type: type of scoring function used in classification
    :param fusion_tree: type of decision tree
    """

    output = open(path_results + 'info', 'w')
    output.write('--------------------------------------------------')
    output.write(str(datetime.datetime.now()))
    output.write('--------------------------------------------------\n\n\n')

    output.write('--------------------------------------------------')
    output.write('MFCC parameters DT')
    output.write('--------------------------------------------------\n')
    output.write('win_length_ms ' + str(mfcc_parameters['win_length_ms']) + '\n')
    output.write('win_shift_ms ' + str(mfcc_parameters['win_shift_ms']) + '\n')
    output.write('n_filters ' + str(mfcc_parameters['n_filters']) + '\n')
    output.write('n_ceps ' + str(mfcc_parameters['n_ceps']) + '\n')
    output.write('f_min ' + str(mfcc_parameters['f_min']) + '\n')
    output.write('f_max ' + str(mfcc_parameters['f_max']) + '\n')
    output.write('delta_win ' + str(mfcc_parameters['delta_win']) + '\n')
    output.write('pre_emphasis_coef ' + str(mfcc_parameters['pre_emphasis_coef']) + '\n')
    output.write('dct_norm ' + str(mfcc_parameters['dct_norm']) + '\n')
    output.write('mel_scale ' + str(mfcc_parameters['mel_scale']) + '\n')
    output.write('with_delta ' + str(mfcc_parameters['with_delta']) + '\n')
    output.write('with_delta_delta  ' + str(mfcc_parameters['with_delta_delta']) + '\n')
    output.write('with_energy  ' + str(mfcc_parameters['with_energy']) + '\n')
    output.write('vad_type  ' + str(mfcc_parameters['vad_type']) + '\n')

    output.write('--------------------------------------------------\n')
    output.write('MFCC parameters GMM')
    output.write('--------------------------------------------------\n')
    output.write('win_length_ms ' + str(mfcc_parameters_fusion['win_length_ms']) + '\n')
    output.write('win_shift_ms ' + str(mfcc_parameters_fusion['win_shift_ms']) + '\n')
    output.write('n_filters ' + str(mfcc_parameters_fusion['n_filters']) + '\n')
    output.write('n_ceps ' + str(mfcc_parameters_fusion['n_ceps']) + '\n')
    output.write('f_min ' + str(mfcc_parameters_fusion['f_min']) + '\n')
    output.write('f_max ' + str(mfcc_parameters_fusion['f_max']) + '\n')
    output.write('delta_win ' + str(mfcc_parameters_fusion['delta_win']) + '\n')
    output.write('pre_emphasis_coef ' + str(mfcc_parameters_fusion['pre_emphasis_coef']) + '\n')
    output.write('dct_norm ' + str(mfcc_parameters_fusion['dct_norm']) + '\n')
    output.write('mel_scale ' + str(mfcc_parameters_fusion['mel_scale']) + '\n')
    output.write('with_delta ' + str(mfcc_parameters_fusion['with_delta']) + '\n')
    output.write('with_delta_delta  ' + str(mfcc_parameters_fusion['with_delta_delta']) + '\n')
    output.write('with_energy  ' + str(mfcc_parameters_fusion['with_energy']) + '\n')
    output.write('vad_type  ' + str(mfcc_parameters_fusion['vad_type']) + '\n')

    output.write('--------------------------------------------------')
    output.write('Decision Tree parameters')
    output.write('--------------------------------------------------\n')
    output.write('tree_type  ' + str(tree_parameters['tree_type']) + '\n')

    output.write('--------------------------------------------------')
    output.write('Fusion parameters')
    output.write('--------------------------------------------------\n')
    output.write('fusion_type  ' + str(fusion_type) + '\n')
    output.write('fusion_tree  ' + str(fusion_tree) + '\n')

    output.close()


def run(path_model_training, path_probe_data, path_results,
        mfcc_parameters, tree_parameters, gmm_parameters, mfcc_parameters_fusion, fusion_type, fusion_tree):
    """
    Function used to run processing based on hybrid algorithm
    :param path_model_training: path to training files
    :param path_probe_data: path to test files
    :param gmm_parameters: dictionary with parameters for GMM algorithm
    :param path_results: path to result of processing, place where file is created
    :param mfcc_parameters: dictionary with mfcc parameters for tree algorithms
    :param tree_parameters: dictionary with tree algorithm parameters
    :param mfcc_parameters_fusion: dictionary with mfcc parameters for GMM algorithm
    :param fusion_type: type of scoring function used in classification
    :param fusion_tree: type of decision tree
    """

    path_results = path_results + 'fusion/' + fusion_tree + '/' + str(datetime.datetime.now())[0:-7] + '/'
    os.makedirs(path_results)

    write_info_parameter_tag(path_results, mfcc_parameters, tree_parameters, mfcc_parameters_fusion, fusion_type,
                             fusion_tree)
    start = str(datetime.datetime.now())
    clf = ''
    if fusion_tree == 'DT':
        clf = train_cart_decision_tree(mfcc_parameters, path_model_training)
    elif fusion_tree == 'RF':
        clf = train_scikit_random_forest(tree_parameters, mfcc_parameters, path_model_training)
    stop = str(datetime.datetime.now())
    path_speaker_models = train_speakers.run_gmm(path_results, path_model_training, mfcc_parameters_fusion,
                                                 gmm_parameters)
    result = 0

    if fusion_type == 'fusion_scale':
        result = test.probe_fusion_scale(clf, mfcc_parameters, path_results, path_probe_data, path_speaker_models,
                                         mfcc_parameters_fusion)
    elif fusion_type == 'fusion_addition':
        result = test.probe_fusion_addition(clf, mfcc_parameters, path_results, path_probe_data, path_speaker_models,
                                            mfcc_parameters_fusion)

    stop2 = str(datetime.datetime.now())
    futil.write_additional_tag_info(path_results, result, start, stop, stop2)
    shutil.rmtree(path_results[:-1] + " " + result + '/models/features', ignore_errors=True)


def train_cart_decision_tree(mfcc_parameters, training):
    """
    Function used to train CART decision tree
    :param mfcc_parameters: dictionary with mfcc parameters
    :param training: path to training files
    """
    file_list = glob.glob(training + "*.wav")
    features, models = mfcc_extraction_dt.get_features(file_list, mfcc_parameters, False)
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(features, models)
    return clf


def train_scikit_random_forest(tree_parameters, mfcc_parameters, training):
    """
    Function used to train random forest
    :param tree_parameters: dictionary with tree algorithms parameters
    :param mfcc_parameters: dictionary with mfcc parameters
    :param training: path to training files
    """
    file_list = glob.glob(training + "*.wav")
    features, models = mfcc_extraction_dt.get_features(file_list, mfcc_parameters, False)
    # clf = ensemble.RandomForestClassifier(n_jobs=2, criterion=criterion_type, n_estimators=forest_size, verbose=1)
    clf = ensemble.RandomForestClassifier(n_estimators=tree_parameters['forest_size'])
    clf = clf.fit(features, models)
    return clf
