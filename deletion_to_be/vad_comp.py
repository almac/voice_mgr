from __future__ import division

__author__ = 'ragnar'
import time

import glob
from vad_algorithms import stat_vad


def get_file_list(path):
    """
    Function creates a dictionary with speaker code as key and sublist of enroll speech files as values
    :param path: path to enrollment set
    :return: :rtype: dictionary of speaker and enrollment set speech files tied to speaker
    """
    file_list = glob.glob(path + "*.wav")

    return file_list


file_list1 = '/home/ragnar/speaker_data/ubm_train/female/'
file_list = get_file_list(file_list1)
print "Computing energy VAD"
# start1 = time.time()
# # for file in file_list:
# #     energy_vad.energy_vad(file)
# pool = ThreadPool(processes=8)
# results = pool.map(energy_vad.energy_vad, file_list)
# pool.close()
# pool.join()
# end1 = time.time()
# delta1= end1-start1
# print str("")
# print str(end1-start1)
print "Computing energy VAD"
start1 = time.time()
results = []
# for file in file_list:
# results.append(energy_vad.energy_vad(file))
# pool = ThreadPool(processes=8)
# results = pool.map(energy_vad.energy_vad, file_list)
# pool.close()
# pool.join()
end1 = time.time()
delta1 = end1 - start1
print str("")
print str(end1 - start1)

print "Computing statistical VAD"
start2 = time.time()
for file in file_list:
    stat_vad.statistic_vad(file)
# pool = ThreadPool(processes=8)
# results = pool.map(stat_vad.statistic_vad, file_list)
# pool.close()
# pool.join()
end2 = time.time()
delta2 = end2 - start2
print str("")
print str(end2 - start2)


# print "Computing statistical VAD"
# start3= time.time()
# for file in file_list:
#     difference_algos.script_energy_vad(file)
# end3 = time.time()
# delta3 =end3-start3
# print str("")
# print str(end3-start3)


print delta1
print delta2
# print delta3

# print delta1/delta2*100

# results = []
# pool = ThreadPool(processes=8)  # ThreadPool(4)
# if mfcc_parameters['vad_type'] == 0:
#     print "Computing energy VAD"
#     results = pool.map(energy_vad.energy_vad, file_list)
#     pool.close()
#     pool.join()
# elif mfcc_parameters['vad_type'] == 1:
#     print "Computing statistical VAD"
#     results = pool.map(stat_vad.statistic_vad, file_list)
#     pool.close()
#     pool.join()