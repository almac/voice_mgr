file = '/home/ragnar/Desktop/energy/f118_13_f11_i0_0.wav'

import heapq
import itertools
import numpy as np
import math
import scipy
import sys
import glob

from vad_algorithms import energy_vad


def second_largest(numbers):
    return heapq.nsmallest(2, numbers)[-1]


def get_file_list(path):
    """
    Function creates a dictionary with speaker code as key and sublist of enroll speech files as values
    :param path: path to enrollment set
    :return: :rtype: dictionary of speaker and enrollment set speech files tied to speaker
    """
    file_list = glob.glob(path + "*.wav")
    splitted_file_list = {}
    for i in file_list:
        new_item = i.split('/enroll/')[1].split('_')
        if new_item[0] in splitted_file_list:
            splitted_file_list[new_item[0]].append(i)
        else:
            splitted_file_list[new_item[0]] = []
            splitted_file_list[new_item[0]].append(i)
    return splitted_file_list


def script_energy_vad(file):
    rate, signal = scipy.io.wavfile.read(str(file))
    signal = np.cast['float'](signal)  # vector should be in **float**
    signal_framed = energy_vad.chunks(signal, 15 * rate / 1000)
    energy = energy_vad.energy_from_signal(signal_framed)
    # energy = energy[0:400]
    energy_max = max(energy)
    energy_min = np.min(np.nonzero(energy))
    aaa = np.array(energy)
    energy_min = np.min(aaa[np.nonzero(aaa)])
    del aaa
    if energy_min == 0:
        energy_min = np.spacing(1)  # min(n for n in energy if n != energy_min)
    threshold1 = energy_min * (1 + 2 * math.log10(energy_max / energy_min))
    sum_energy = 0.0
    count_energy = 0
    for e in energy:
        if e >= threshold1:
            sum_energy += e
            count_energy += 1
    sl = sum_energy / count_energy
    # sl = np.mean(energy)
    threshold2 = threshold1 + 0.25 * (sl - threshold1)
    start = False
    word = False
    f_start = 0
    f_stop = 0
    word_index = []
    #
    # energy = [0,0,0,0,0,8,100,100,1000,3,20,1000,100,10,2,0,0,0,0,0]
    # threshold1 = 7
    # threshold2 = 30
    for i in range(len(energy)):
        cur_e = energy[i]
        if threshold2 >= energy[i] >= threshold1:
            if not start:
                start = True
                f_start = i
        if energy[i] >= threshold2:
            if start:
                word = True
        if energy[i] <= threshold1:
            if start:
                if not word:
                    start = False
                if word:
                    word = False
                    start = False
                    f_stop = i
                    word_index.append([f_start, f_stop])


    # print word_index
    indexes = []
    for a in word_index:
        for i in range(a[0], a[1], 1):
            indexes.append(i)


    # print indexes
    signal_framed = [v for i, v in enumerate(signal_framed) if energy[i] > threshold2]
    last_sig = list(itertools.chain.from_iterable(signal_framed))
    last_sig = np.int16(last_sig)
    sys.stdout.write('.')
    return last_sig

#
#
# scipy.io.wavfile.write('/home/ragnar/Desktop/silenced_stat_moj.wav', rate, last_sig)
#
# win_length_ms = 30  # The window length of the cepstral analysis in milliseconds
# win_shift_ms = 10  # The window shift of the cepstral analysis in milliseconds
# n_filters = 44  # The number of filter bands
# n_ceps = 28  # The number of cepstral coefficients
# f_min = 0.  # The minimal frequency of the filter bank
# f_max = 8000.  # The maximal frequency of the filter bank
# delta_win = 2  # The integer delta value used for computing the first and second order derivatives
# pre_emphasis_coef = 0.97  # The coefficient used for the pre-emphasis
# dct_norm = True  # A factor by which the cepstral coefficients are multiplied
# mel_scale = True  # Tell whether cepstral features are extracted on a linear (LFCC) or Mel (MFCC) scale
# with_delta = True  # If MFCC deltas are computed
# with_delta_delta = True  # If MFCC deltas are computed
# with_energy = True  # if another energy feature MFCC[0]
# vad_type = 0  # if 0 energy vad if 1 stat vad
#
# mfcc_parameters = {'win_length_ms': win_length_ms, 'win_shift_ms': win_shift_ms, 'n_filters': n_filters,
# 'n_ceps': n_ceps, 'f_min': f_min, 'f_max': f_max, 'delta_win': delta_win,
#                    'pre_emphasis_coef': pre_emphasis_coef, 'dct_norm': dct_norm, 'mel_scale': mel_scale,
#                    'with_delta': with_delta, 'with_delta_delta': with_delta_delta, 'with_energy': with_energy,
#                    'vad_type': vad_type}
#
# c = bob.ap.Ceps(16000, mfcc_parameters['win_length_ms'], mfcc_parameters['win_shift_ms'],
#                 mfcc_parameters['n_filters'], mfcc_parameters['n_ceps'], mfcc_parameters['f_min'],
#                 mfcc_parameters['f_max'], mfcc_parameters['delta_win'], mfcc_parameters['pre_emphasis_coef'],
#                 mfcc_parameters['mel_scale'], mfcc_parameters['dct_norm'])
# c.with_delta = mfcc_parameters['with_delta']
# c.with_delta_delta = mfcc_parameters['with_delta_delta']
# c.with_energy = mfcc_parameters['with_energy']
#
# mfcc = c(last_sig)
#
# print mfcc
#
#
#
# #
# # res = '/home/ragnar/Desktop/test/'
# # modela = '/home/ragnar/Desktop/test/models/'
# # path_probe_data = '/home/ragnar/Desktop/test/probe/'
# # import file_utils
# # import mfcc_extraction
# # import math
# # import time
# # import csv
# # import bob.io.base
# # import bob.learn.misc
# #
# # ubm = '/home/ragnar/Desktop/test/ubm_model.hdf5'
# # models_list = file_utils.get_file_list(modela, 'hdf5')
# # files_list = file_utils.get_file_list(path_probe_data, 'hdf5')
# # ubm_file = bob.io.base.HDF5File(ubm, 'r')
# # ubm_model = bob.learn.misc.GMMMachine(ubm_file)
# #
# # files_list.sort()
# # models_list.sort()
# # # files_list = files_list[0:50]
# # models = []
# #
# # for gmm_file in models_list:
# # gmm_hdf5_file = bob.io.base.HDF5File(gmm_file, 'r')
# # models.append(bob.learn.misc.GMMMachine(gmm_hdf5_file))
# #     del gmm_hdf5_file
# #
# # resultFile = open(res + "results.csv", 'wb')
# # wr = csv.writer(resultFile, dialect='excel')
# # start_row = ['']
# #
# # i = 1
# # j = 0
# #
# # for model in models_list:
# #     start_row.append(model.split("_")[1][0:-5])
# # start_row.append('decision')
# # wr.writerow(start_row)
# #
# # for feature_file in files_list:
# #     features = file_utils.open_features_file(feature_file)
# #
# #     print "Processing progress " + str(i) + "/" + str(len(files_list))
# #     results_file = []
# #     results_feat = [0] * len(models_list)
# #     results = [0] * len(models_list)
# #     results_file.append(feature_file.split('.')[0])
# #     for model_no in range(0, len(models_list)):
# #         loglikelihood = 0
# #
# #         for feature_vector_no in range(0, len(features)):
# #             loglikelihood += models[model_no](features[feature_vector_no])-ubm_model(features[feature_vector_no])
# #         results_file.append(int(loglikelihood))
# #
# #     for feature_vector_no in range(0, len(features)):
# #         for model_no in range(0, len(models_list)):
# #             results_feat[model_no] = models[model_no](features[feature_vector_no])
# #         results[results_feat.index(max(results_feat))] += 1
# #
# #     '''
# #     Finds model name connected with max LogLikelihood
# #
# #     max(results_file[1:]) - search max value in loglikelihood results for processed file
# #     results_file.index(...) - gets index of max value of LogLikelihood for processed file
# #     models_list[].split("_")[1][0:-5] - gets name of model
# #     '''
# #
# #     model_name = models_list[results_file[1:].index(max(results_file[1:]))].split("_")[1][0:-5]
# #     results_file.append(model_name)
# #
# #     if model_name in feature_file:
# #         results_file.append(1)
# #         j += 1
# #     else:
# #         results_file.append(0)
# #
# #     wr.writerow(results_file)
# #
# #     i += 1
# #
# # percentage = j
# # wr.writerow([percentage])
# # resultFile.close()
# # stop = time.time()
#
#
