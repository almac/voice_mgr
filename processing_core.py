__author__ = 'Maciej Aleszczyk'

from gmm_ubm import test_gmm_ubm as gmmubm
from gmm_ubm import test_gmm as gmm
from random_forest import test_random_forest as rf
from decision_tree import test_decision_tree as dt
from rule_induction import test_rule_induction as ri
from fusion import test_fusion_dt_ubm as fs
from gradient_tree import test_gradient_tree as gbrt


def processing_info(processing_type, tree_type):
    """
    Function used write which algorithm was used in current processing
    """

    if processing_type in ("RF", "DT"):
        print "Processing " + processing_type + " " + tree_type
    else:
        print "Processing " + processing_type


def run(processing_type,
        path_ubm_training, path_model_training, path_probe_data, path_results,
        win_length_ms, win_shift_ms, n_filters, n_ceps, f_min, f_max, delta_win, pre_emphasis_coef, dct_norm,
        mel_scale, with_delta, with_delta_delta, with_energy, vad_type,
        relevance_factor, convergence_threshold, max_iterations, gaussians_no,
        tree_type, criterion_type, forest_size,
        fusion_type, fusion_tree,
        estimators, max_depth, learing_rate):

    """
    Function used to run whole speaker recognition processing, setup dictionaries with parameters and choose
    classification algorithm
    """
    mfcc_parameters = {'win_length_ms': win_length_ms[0], 'win_shift_ms': win_shift_ms[0], 'n_filters': n_filters[0],
                       'n_ceps': n_ceps[0], 'f_min': f_min[0], 'f_max': f_max[0], 'delta_win': delta_win[0],
                       'pre_emphasis_coef': pre_emphasis_coef[0], 'dct_norm': dct_norm[0], 'mel_scale': mel_scale[0],
                       'with_delta': with_delta[0], 'with_delta_delta': with_delta_delta[0],
                       'with_energy': with_energy[0],
                       'vad_type': vad_type[0]}

    gmm_parameters = {'relevance_factor': relevance_factor, 'convergence_threshold': convergence_threshold,
                      'max_iterations': max_iterations, 'gaussians_no': gaussians_no, }

    tree_parameters = {'tree_type': tree_type, 'criterion_type': criterion_type, 'forest_size': forest_size}

    gbrt_parameters = {'estimators': estimators, 'max_depth': max_depth, 'learning_rate': learing_rate}

    if processing_type == "GMMUBM":
        processing_info(processing_type, tree_type)
        gmmubm.run(path_ubm_training, path_model_training, path_probe_data, path_results,
                   mfcc_parameters, gmm_parameters)

    elif processing_type == "GMM":
        processing_info(processing_type, tree_type)
        gmm.run(path_model_training, path_probe_data, path_results,
                mfcc_parameters, gmm_parameters)

    elif processing_type == "RF":
        processing_info(processing_type, tree_type)
        rf.run(path_model_training, path_probe_data, path_results,
               mfcc_parameters, tree_parameters)

    elif processing_type == "DT":
        processing_info(processing_type, tree_type)
        dt.run(path_model_training, path_probe_data, path_results,
               mfcc_parameters, tree_parameters)

    elif processing_type == "RI":
        processing_info(processing_type, tree_type)
        ri.run(path_model_training, path_probe_data, path_results,
               mfcc_parameters)

    elif processing_type == "FUSION":

        mfcc_parameters_fusion = {'win_length_ms': win_length_ms[1], 'win_shift_ms': win_shift_ms[1],
                                  'n_filters': n_filters[1],
                                  'n_ceps': n_ceps[1], 'f_min': f_min[1], 'f_max': f_max[1], 'delta_win': delta_win[1],
                                  'pre_emphasis_coef': pre_emphasis_coef[1], 'dct_norm': dct_norm[1],
                                  'mel_scale': mel_scale[1],
                                  'with_delta': with_delta[1], 'with_delta_delta': with_delta_delta[1],
                                  'with_energy': with_energy[1],
                                  'vad_type': vad_type[1]}

        processing_info(processing_type, tree_type)
        fs.run(path_model_training, path_probe_data, path_results,
               mfcc_parameters, tree_parameters, gmm_parameters, mfcc_parameters_fusion,
               fusion_type, fusion_tree)

    elif processing_type == "GBRT":

        processing_info(processing_type, tree_type)
        gbrt.run(path_model_training, path_probe_data, path_results,
               mfcc_parameters, gbrt_parameters)