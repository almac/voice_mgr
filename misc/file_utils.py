__author__ = 'Maciej Aleszczyk'

import glob
import os
import time

import bob.io.base


def write_additional_tag_info(path_results, result, start, stop, stop2):
    output = open(path_results[:-1] + " " + result + '/info', 'a')
    output.write('--------------------------------------------------')
    output.write('Results')
    output.write('--------------------------------------------------\n')
    output.write("Correct %   " + result + '\n')
    output.write("Start training model   " + start + '\n')
    output.write("Finish training model  " + stop + '\n')
    output.write("Finish processing  " + stop2 + '\n')
    output.close()


def get_file_list(path, extension):
    """
    Function used to get .* files from specific path
    :param extension: extemsion of desired filetype f.e.wav
    :param path: path to directory with MFCC files
    :return: :rtype: list of files
    """
    file_list = glob.glob(path + "*." + extension)
    return file_list


def open_features_file(path):
    """
    Function opens hdf5 file with MFCC features
    :param path: path to hdf5 file
    :return: :rtype: features list
    """
    print "Opening HDF5 File"
    start = time.time()
    mfcc_file = bob.io.base.HDF5File(path)
    features = mfcc_file.read('features')
    stop = time.time()
    del mfcc_file
    print "Opened and read in " + str(stop - start) + " s"
    return features


def save_gmm(path, gmm):
    """
    Function saves GMM in specific location as hdf5 file
    :param path: path to save GMM
    :param gmm: Gaussian Mixture Model to be saved
    """
    print "Saving to HDF5 File"
    start = time.time()

    if not os.path.isdir(path[0:path.rfind('/')]):
        os.makedirs(path[0:path.rfind('/')])

    f = bob.io.base.HDF5File(path, 'w')
    f.set('weights', gmm.weights)
    f.set('means', gmm.means)
    f.set('variances', gmm.variances)
    del f
    stop = time.time()
    print "Saved in " + str(stop - start) + " s"
    print "-----------------------------------"


def open_gmm(path, gmm_parameters):
    """
    Function opens hdf5 file with saved GMM and returns it
    :param path: path to GMM hdf5 file
    :return: :rtype: GMM bob object
    """
    print "Opening HDF5 File"
    start = time.time()
    gmm_file = bob.io.base.HDF5File(path)
    gmm = bob.learn.misc.GMMMachine(gmm_parameters['gaussians_no'], gmm_parameters['features'])
    gmm.weights = gmm_file.read('weights')
    gmm.means = gmm_file.read('means')
    gmm.variances = gmm_file.read('variances')
    del gmm_file
    stop = time.time()
    print "Opened and read in " + str(stop - start) + " s"
    return gmm