from __future__ import division
import glob
import datetime
import os

import Orange

from feature_extraction import mfcc_extraction_dt
from misc import testing_utils as test
from misc import file_utils as futil


def write_info_parameter_tag(path_results, mfcc_parameters):
    """
    Function creates info file containing parameters used in current processing

    :param path_results: path to result of processing, place where file is created
    :param mfcc_parameters: parameters of mfcc processing
    """
    output = open(path_results + 'info', 'w')
    output.write('--------------------------------------------------')
    output.write(str(datetime.datetime.now()))
    output.write('--------------------------------------------------\n\n\n')
    output.write('--------------------------------------------------')
    output.write('MFCC parameters')
    output.write('--------------------------------------------------\n')
    output.write('win_length_ms ' + str(mfcc_parameters['win_length_ms']) + '\n')
    output.write('win_shift_ms ' + str(mfcc_parameters['win_shift_ms']) + '\n')
    output.write('n_filters ' + str(mfcc_parameters['n_filters']) + '\n')
    output.write('n_ceps ' + str(mfcc_parameters['n_ceps']) + '\n')
    output.write('f_min ' + str(mfcc_parameters['f_min']) + '\n')
    output.write('f_max ' + str(mfcc_parameters['f_max']) + '\n')
    output.write('delta_win ' + str(mfcc_parameters['delta_win']) + '\n')
    output.write('pre_emphasis_coef ' + str(mfcc_parameters['pre_emphasis_coef']) + '\n')
    output.write('dct_norm ' + str(mfcc_parameters['dct_norm']) + '\n')
    output.write('mel_scale ' + str(mfcc_parameters['mel_scale']) + '\n')
    output.write('with_delta ' + str(mfcc_parameters['with_delta']) + '\n')
    output.write('with_delta_delta  ' + str(mfcc_parameters['with_delta_delta']) + '\n')
    output.write('with_energy  ' + str(mfcc_parameters['with_energy']) + '\n')
    output.write('vad_type  ' + str(mfcc_parameters['vad_type']) + '\n')
    output.close()


def run(path_model_training, path_probe_data, path_results,
        mfcc_parameters):
    """
    Function used to start speaker recognition process based on rule induction algorithm
    :param path_model_training: path to training files
    :param path_probe_data: path to test files
    :param path_results: path to result of processing, place where file is created
    :param mfcc_parameters: dictionary with mfcc parameters
    """

    path_results = path_results + 'rule_induction/' + str(datetime.datetime.now())[0:-7] + '/'
    os.makedirs(path_results)

    write_info_parameter_tag(path_results, mfcc_parameters)

    start = str(datetime.datetime.now())
    clf, domain = train_rules(mfcc_parameters, path_model_training)
    stop = str(datetime.datetime.now())
    result = test.probe_orange(clf, domain, mfcc_parameters, path_results, path_probe_data)
    stop2 = str(datetime.datetime.now())
    futil.write_additional_tag_info(path_results, result, start, stop, stop2)


def train_rules(mfcc_parameters, training):
    """
    Function used to perform rules induction
    :param mfcc_parameters: dictionary with mfcc parameters
    :param training: path to training files
    """

    file_list = glob.glob(training + "*.wav")
    features = mfcc_extraction_dt.get_features(file_list, mfcc_parameters, True)

    feat = [Orange.feature.Continuous('a%i' % x) for x in range(len(features[1]) - 1)]

    clazz = ['f118', 'f122', 'f123', 'f125', 'f126', 'f127', 'f128', 'f129', 'f130', 'f131', 'f133', 'f302',
             'f303', 'f306', 'f307', 'f309', 'f311', 'f320']
    class_ = [Orange.feature.Discrete('class', values=clazz)]
    domain = Orange.data.Domain(feat + class_, clazz)
    data = Orange.data.Table(domain, features)

    clf = Orange.classification.rules.CN2Learner(data)

    return clf, domain


