__author__ = 'Maciej Aleszczyk'
import scipy.io.wavfile
import numpy as np
import sys


def chunks(l, n):
    """
    Cut signal into n-length parts
    :param l: signal to be chunked
    :type l: int
    :param n: chunk length
    :type n: int
    :return: chunked signal
    :rtype: list
    """
    if n < 1:
        n = 1
    return [l[i:i + n] for i in range(0, len(l), n)]


def statistic_vad(wave_path, win_length_ms_vad=10, silence_region_ms=100):
    """
    Computes statistic VAD from file
    :param win_length_ms_vad: the window length of the VAD analysis in milliseconds
    :param silence_region_ms: hypothetical silence region in signal in milliseconds
    :param wave_path: path to the file
    """

    rate, signal = scipy.io.wavfile.read(str(wave_path))
    signal = np.cast['float'](signal)  # vector should be in **float**
    # # get silence region of samples
    silence_signal = signal[0:rate * silence_region_ms / 1000]
    mean = np.mean(silence_signal)
    std_dev = np.std(silence_signal)

    chunked_signal = chunks(signal, win_length_ms_vad * rate / 1000)
    last_sig = []
    for slice in chunked_signal:
        ones = 0
        zeros = 0
        for sample in slice:
            if abs(sample - mean) / std_dev > 3:
                ones += 1
            else:
                zeros += 1
        if zeros <= ones:
            last_sig.extend(slice)
        del ones
        del zeros
        del slice

    del chunked_signal
    del signal

    sys.stdout.write('.')
    return {'samples': last_sig, 'file': wave_path.split('_')[0][-3:]}
