__author__ = 'Maciej Aleszczyk'

from multiprocessing.dummy import Pool as ThreadPool
import time
import os
import sys
import numpy as np
from scipy.signal import lfilter
import scipy

import bob.ap
import bob.io.base
import bob.learn.misc
from scikits.talkbox import lpc

from vad_algorithms import stat_vad, energy_vad


def get_formants(params):
    """
    Function used to obtain formants or LPC features
    :param params: wav file samples
    :return: array with computed features
    """

    window_len = int(c.win_length_ms * c.sampling_frequency / 1000)
    window_step = int(c.win_shift_ms * c.sampling_frequency / 1000)
    frames = np.array([params[i:i + window_len] for i in range(0, len(params) - window_len + 1, window_step)])
    results = []

    for step in frames:
        w = np.hamming(len(step))

        x1 = step * w
        x1 = lfilter([1], [1., 0.63], x1)

        ncoeff = 2 + 16000 / 1000
        A, e, k = lpc(x1, ncoeff)

        rts = np.roots(A)
        rts = [r for r in rts if np.imag(r) >= 0.1]

        # angz = np.arctan2(np.imag(rts), np.real(rts))

        # frqs = sorted(angz * (16000 / (2 * math.pi)))
        # results.append(frqs[1:3])
        results.append(np.real(rts)[:5])
        del rts
        # del frqs

    del frames
    return np.array(results)


def compute_mfcc(params):
    """
    Function computes MFCC features from given wav file samples which are sampled and with deleted silence
    :param params: wav file samples which are already framed
    :return: dictionary with mfcc features and speaker name
    """
    mfcc = c(params['samples'])
    # formants = get_formants(params['samples'])

    # results = np.ndarray(shape=(len(mfcc[0]), 95))
    #
    # for i in range(0, len(mfcc)):
    # results[i] = np.append(mfcc[i], formants[i])
    sys.stdout.write('.')
    # del mfcc
    # del formants
    return {'mfcc': mfcc, 'file': params['file']}


def get_features(file_list, mfcc_parameters, only_mfcc=True):
    """
    Function computes MFCC parameters from given file list
    :param file_list: file list from which MFCC parameters will be computed
    :param only_mfcc: if return only mfcc features or information about speaker
    :param mfcc_parameters: MFCC computation settings
    :return: dictionary with mfcc features and speaker name or mfcc features list
    """
    print "Number of processing files: " + str(len(file_list))

    print "-----------------------------------"
    start = time.time()
    results = []
    pool = ThreadPool(processes=8)  # ThreadPool(4)
    if mfcc_parameters['vad_type'] == 0:
        print "Computing energy VAD"
        results = pool.map(energy_vad.energy_vad, file_list)
        pool.close()
        pool.join()
    elif mfcc_parameters['vad_type'] == 1:
        print "Computing statistical VAD"
        results = pool.map(stat_vad.statistic_vad, file_list)
        pool.close()
        pool.join()
    else:
        signals = []
        for wav_file in file_list:
            rate, signal = scipy.io.wavfile.read(str(wav_file))
            signals.append(signal)
            sys.stdout.write('.')

        signals = np.cast['float'](signals)
        results['samples'].append(signals)
        results = np.array(results)

    end = time.time()

    print "\nVAD computed in " + str(end - start) + " s"
    print "-----------------------------------"

    global c
    c = bob.ap.Ceps(16000, mfcc_parameters['win_length_ms'], mfcc_parameters['win_shift_ms'],
                    mfcc_parameters['n_filters'], mfcc_parameters['n_ceps'], mfcc_parameters['f_min'],
                    mfcc_parameters['f_max'], mfcc_parameters['delta_win'], mfcc_parameters['pre_emphasis_coef'],
                    mfcc_parameters['mel_scale'], mfcc_parameters['dct_norm'])

    c.with_delta = mfcc_parameters['with_delta']
    c.with_delta_delta = mfcc_parameters['with_delta_delta']
    c.with_energy = mfcc_parameters['with_energy']

    print "Extracting MFCC features"
    start = time.time()
    pool = ThreadPool(processes=8)
    features = pool.map(compute_mfcc, results)
    pool.close()
    pool.join()
    end = time.time()

    del results

    print "\nMFCC features extracted in " + str(end - start) + " s"
    print "-----------------------------------"
    start = time.time()
    if only_mfcc:
        mfcc = []
        for i in range(0, len(features)):
            mfcc.extend(features[i]['mfcc'])
        end = time.time()
        print "Feature list transformation " + str(end - start)
        print "-----------------------------------"
        return mfcc
    return features


def get_features_file(wav_file, mfcc_parameters, only_mfcc=True):
    """
    Function computes MFCC features from file
    :param wav_file: wav file from which MFCC
    :param mfcc_parameters: MFCC computation settings
    :param only_mfcc: if return only mfcc features or information about files
    :return: dictionary with mfcc features and speaker name or mfcc features list
    """

    print "-----------------------------------"
    start = time.time()
    results = []
    if mfcc_parameters['vad_type'] == 0:
        print "Computing energy VAD"
        results = energy_vad.energy_vad(wav_file)
    elif mfcc_parameters['vad_type'] == 1:
        print "Computing statistical VAD"
        results = stat_vad.statistic_vad(wav_file)
    else:
        rate, signal = scipy.io.wavfile.read(str(wav_file))
        signal = np.cast['float'](signal)
        results = {'samples': signal}
    end = time.time()

    print "\nVAD computed in " + str(end - start) + " s"
    print "-----------------------------------"

    global c
    c = bob.ap.Ceps(16000, mfcc_parameters['win_length_ms'], mfcc_parameters['win_shift_ms'],
                    mfcc_parameters['n_filters'], mfcc_parameters['n_ceps'], mfcc_parameters['f_min'],
                    mfcc_parameters['f_max'], mfcc_parameters['delta_win'], mfcc_parameters['pre_emphasis_coef'],
                    mfcc_parameters['mel_scale'], mfcc_parameters['dct_norm'])
    c.with_delta = mfcc_parameters['with_delta']
    c.with_delta_delta = mfcc_parameters['with_delta_delta']
    c.with_energy = mfcc_parameters['with_energy']

    print "Extracting MFCC features"
    start = time.time()
    features = compute_mfcc(results)

    end = time.time()

    print "\nMFCC features extracted in " + str(end - start) + " s"
    print "-----------------------------------"
    start = time.time()
    if only_mfcc:
        mfcc = []
        mfcc.extend(features['mfcc'])
        end = time.time()
        print "Feature list transformation " + str(end - start)
        print "-----------------------------------"
        return mfcc
    return features


def save_features(path, features):
    """
    Function saves computed features to specific directory
    :param path: directory where hdf5 file MFCC features will be saved
    :param features: MFCC features to be saved
    """
    print "Saving to HDF5 File"
    start = time.time()

    if not os.path.isdir(path[0:path.rfind('/')]):
        os.makedirs(path[0:path.rfind('/')])

    f = bob.io.base.HDF5File(path, 'w')
    f.append('features', features)
    del f
    stop = time.time()
    print "Saved in " + str(stop - start) + " s"
    print "-----------------------------------"


def open_features_file(path):
    """
    Function opens hdf5 file with MFCC features
    :param path: path to hdf5 file
    :return: :rtype: features list
    """
    print "Opening HDF5 File"
    start = time.time()
    mfcc_file = bob.io.base.HDF5File(path)
    features = mfcc_file.read('features')
    stop = time.time()
    del mfcc_file
    print "Opened and read in " + str(stop - start) + " s"
    return features


