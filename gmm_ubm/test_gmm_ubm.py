__author__ = 'Maciej Aleszczyk'

import datetime
import os
import shutil

from gmm_ubm import prepare_test_data, recognition_test, train_speakers, train_ubm


def write_additional_tag_info_gmmubm(path_results, result, start, stop, stop2, stop3):
    """
    Function used to write additional information to info file after end of processing
    :param path_results: path to directory containing current processing files
    :param result: percentage result of positively recognised speakers
    :param start: start of training GMMUBM model
    :param stop: finish of training GMMUBM model
    :param stop2: finish creating MAP models
    :param stop3: finish of processing
    """

    output = open(path_results[:-1] + " " + result + '/info', 'a')
    output.write('--------------------------------------------------')
    output.write('Results')
    output.write('--------------------------------------------------\n')
    output.write("Correct %   " + result + '\n')
    output.write("Start training GMMUBM model   " + start + '\n')
    output.write("Finish training GMMUBM model  " + stop + '\n')
    output.write("Finish creating MAP models  " + stop2 + '\n')
    output.write("Finish processing  " + stop3 + '\n')
    output.close()


def write_info_parameter_tag(path_results, mfcc_parameters, gmm_parameters):
    """
    Function creates info file containing parameters used in current processing

    :param path_results: path to result of processing, place where file is created
    :param mfcc_parameters: parameters of mfcc processing
    :param gmm_parameters: parameters of gmm processing
    """
    output = open(path_results + 'info', 'w')
    output.write('--------------------------------------------------')
    output.write(str(datetime.datetime.now()))
    output.write('--------------------------------------------------\n\n\n')
    output.write('--------------------------------------------------')
    output.write('MFCC parameters')
    output.write('--------------------------------------------------\n')
    output.write('win_length_ms ' + str(mfcc_parameters['win_length_ms']) + '\n')
    output.write('win_shift_ms ' + str(mfcc_parameters['win_shift_ms']) + '\n')
    output.write('n_filters ' + str(mfcc_parameters['n_filters']) + '\n')
    output.write('n_ceps ' + str(mfcc_parameters['n_ceps']) + '\n')
    output.write('f_min ' + str(mfcc_parameters['f_min']) + '\n')
    output.write('f_max ' + str(mfcc_parameters['f_max']) + '\n')
    output.write('delta_win ' + str(mfcc_parameters['delta_win']) + '\n')
    output.write('pre_emphasis_coef ' + str(mfcc_parameters['pre_emphasis_coef']) + '\n')
    output.write('dct_norm ' + str(mfcc_parameters['dct_norm']) + '\n')
    output.write('mel_scale ' + str(mfcc_parameters['mel_scale']) + '\n')
    output.write('with_delta ' + str(mfcc_parameters['with_delta']) + '\n')
    output.write('with_delta_delta  ' + str(mfcc_parameters['with_delta_delta']) + '\n')
    output.write('with_energy  ' + str(mfcc_parameters['with_energy']) + '\n')
    output.write('vad_type  ' + str(mfcc_parameters['vad_type']) + '\n')
    output.write('\n')
    output.write('\n')
    output.write('--------------------------------------------------')
    output.write('GMM parameters')
    output.write('--------------------------------------------------\n')
    output.write('relevance_factor ' + str(gmm_parameters['relevance_factor']) + '\n')
    output.write('convergence_threshold ' + str(gmm_parameters['convergence_threshold']) + '\n')
    output.write('max_iterations ' + str(gmm_parameters['max_iterations']) + '\n')
    output.write('gaussians_no ' + str(gmm_parameters['gaussians_no']) + '\n')
    output.close()


def run(path_ubm_training, path_model_training, path_probe_data, path_results,
        mfcc_parameters, gmm_parameters):
    """
    Main function of processing cycle.
        Steps of processing:
            1. Obtaining UBM model
            2. Obtaining MAP-GMM models from adaptation on UBM model
            3. Preparing test data to check classification correctness
            4. Obtain log-likelihood distance with each speaker model. Creating output file with recognition results.

    :param path_results: path to directory containing current processing files
    :param path_probe_data: path to test files
    :param path_model_training: path to training files
    :param mfcc_parameters: dictionary with mfcc processing settings
    :param gmm_parameters: dictionary with gmm processing settings
    """

    path_results = path_results + 'gmm_ubm/' + str(datetime.datetime.now())[0:-7] + '/'

    os.makedirs(path_results)

    write_info_parameter_tag(path_results, mfcc_parameters, gmm_parameters)

    start = str(datetime.datetime.now())
    path_ubm_model = train_ubm.run(path_results, path_ubm_training, mfcc_parameters, gmm_parameters)
    stop = str(datetime.datetime.now())
    path_speaker_models = train_speakers.run(path_results, path_ubm_model, path_model_training, mfcc_parameters,
                                             gmm_parameters)
    stop2 = str(datetime.datetime.now())
    path_test_data = prepare_test_data.run(path_results, path_probe_data, mfcc_parameters)
    result = recognition_test.run(path_results, path_speaker_models, path_test_data)
    stop3 = str(datetime.datetime.now())

    write_additional_tag_info_gmmubm(path_results, result, start, stop, stop2, stop3)

    shutil.rmtree(path_results[:-1] + " " + result + '/probe', ignore_errors=True)
    shutil.rmtree(path_results[:-1] + " " + result + '/models/features', ignore_errors=True)


