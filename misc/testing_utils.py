from __future__ import division

__author__ = 'ragnar'

import glob
import os
import csv

import Orange

from feature_extraction import mfcc_extraction_dt
from feature_extraction import mfcc_extraction
from misc import file_utils
import bob.io.base
import bob.learn.misc


def probe_scikit(clf, mfcc_parameters, path_results, probe):
    """
    Function used to conduct classification for decision tree/random forest created with scikit-learn library
    :param clf: classifier to be tested
    :param mfcc_parameters: dictionary with mfcc parameters
    :param path_results: path to directory containing current processing files
    :param probe: path to test files
    """

    test_files = glob.glob(probe + "*.wav")
    test_files.sort()
    print len(test_files)
    start_row = ['']
    model_list = ['f118', 'f122', 'f123', 'f125', 'f126', 'f127', 'f128', 'f129', 'f130', 'f131', 'f133', 'f302',
                  'f303', 'f306', 'f307', 'f309', 'f311', 'f320']
    for i in range(0, len(model_list)):
        start_row.append(model_list[i])
    start_row.append('decision')
    result_file_csv = open(path_results + "results.csv", 'wb')
    wr = csv.writer(result_file_csv, dialect='excel')
    wr.writerow(start_row)
    score = 0
    point = 1
    for processing_file in test_files:
        features, models = mfcc_extraction_dt.get_features_file(processing_file, mfcc_parameters, False)

        print "Processing progress " + str(point) + "/" + str(len(test_files))
        processing_file_results = [processing_file]
        results = {}
        for i in range(0, len(model_list)):
            results[model_list[i]] = 0

        for i in range(0, len(features)):
            key = str(clf.predict(features[i].reshape(1, -1))[0])
            results[key] += 1

        for key in sorted(results):
            processing_file_results.append(results[key])

        result_max = str(max(results, key=results.get))
        if result_max in processing_file:
            processing_file_results.append(1)
            score += 1
        else:
            processing_file_results.append(0)

        wr.writerow(processing_file_results)
        point += 1
    percentage = score / len(test_files) * 100
    wr.writerow([percentage])
    result_file_csv.close()
    percentage_str = str(round(percentage, 3)) + "%"
    os.rename(path_results[:-1], path_results[:-1] + " " + percentage_str)
    return percentage_str


def probe_scikit_separate(clf, mfcc_parameters, path_results, probe):
    """
    Function used to conduct classification for separate decision tree for each speaker
    :param clf: classifier to be tested
    :param mfcc_parameters: dictionary with mfcc parameters
    :param path_results: path to directory containing current processing files
    :param probe: path to test files
    """

    test_files = glob.glob(probe + "*.wav")
    test_files.sort()
    print len(test_files)
    start_row = ['']
    model_list = ['f118', 'f122', 'f123', 'f125', 'f126', 'f127', 'f128', 'f129', 'f130', 'f131', 'f133', 'f302',
                  'f303', 'f306', 'f307', 'f309', 'f311', 'f320']
    for i in range(0, len(model_list)):
        start_row.append(model_list[i])
    start_row.append('decision')
    result_file_csv = open(path_results + "results.csv", 'wb')
    wr = csv.writer(result_file_csv, dialect='excel')
    wr.writerow(start_row)
    score = 0
    point = 1
    for processing_file in test_files:
        features, models = mfcc_extraction_dt.get_features_file(processing_file, mfcc_parameters, False)

        print "Processing progress " + str(point) + "/" + str(len(test_files))
        processing_file_results = [processing_file]
        results = {}
        for i in range(0, len(model_list)):
            results[model_list[i]] = 0
        results["none"] = 0
        for i in range(0, len(features)):
            for clf_key in clf.keys():
                key = str(clf[clf_key].predict(features[i].reshape(1, -1))[0])
                results[key] += 1

        results["none"] = 0
        for key in sorted(results):
            processing_file_results.append(results[key])

        result_max = str(max(results, key=results.get))
        if result_max in processing_file:
            processing_file_results.append(1)
            score += 1
        else:
            processing_file_results.append(0)

        wr.writerow(processing_file_results)
        point += 1
    percentage = score / len(test_files) * 100
    wr.writerow([percentage])
    result_file_csv.close()
    percentage_str = str(round(percentage, 3)) + "%"
    os.rename(path_results[:-1], path_results[:-1] + " " + percentage_str)
    return percentage_str


def probe_orange(clf, domain, mfcc_parameters, path_results, probe):
    """
    Function used to conduct classification for decision tree/random forest created with Orange library
    :param clf: classifier to be tested
    :param mfcc_parameters: dictionary with mfcc parameters
    :param path_results: path to directory containing current processing files
    :param probe: path to test files
    """

    test_files = glob.glob(probe + "*.wav")
    test_files.sort()
    print len(test_files)
    start_row = ['']
    model_list = ['f118', 'f122', 'f123', 'f125', 'f126', 'f127', 'f128', 'f129', 'f130', 'f131', 'f133', 'f302',
                  'f303', 'f306', 'f307', 'f309', 'f311', 'f320']
    for i in range(0, len(model_list)):
        start_row.append(model_list[i])
    start_row.append('decision')
    result_file_csv = open(path_results + "results.csv", 'wb')
    wr = csv.writer(result_file_csv, dialect='excel')
    wr.writerow(start_row)
    score = 0
    point = 1

    for processing_file in test_files:
        features = mfcc_extraction_dt.get_features_file(processing_file, mfcc_parameters, True)

        print "Processing progress " + str(point) + "/" + str(len(test_files))
        processing_file_results = [processing_file]
        results = {}
        for i in range(0, len(model_list)):
            results[model_list[i]] = 0

        data = Orange.data.Table(domain, features)

        for i in range(0, len(data)):
            key = str(clf(data[i]))
            results[key] += 1

        for key in sorted(results):
            processing_file_results.append(results[key])

        result_max = str(max(results, key=results.get))
        if result_max in processing_file:
            processing_file_results.append(1)
            score += 1
        else:
            processing_file_results.append(0)

        wr.writerow(processing_file_results)
        point += 1

        del results
        del data
        del features

    percentage = score / len(test_files) * 100
    wr.writerow([percentage])
    result_file_csv.close()
    percentage_str = str(round(percentage, 3)) + "%"
    os.rename(path_results[:-1], path_results[:-1] + " " + percentage_str)

    del clf

    return percentage_str


def probe_fusion_scale(clf, mfcc_parameters, path_results, probe, path_speaker_models, mfcc_parameters_fusion):
    """
    Function used to perform classification test for hybrid algorithm when fusion_scale scoring method was chosen
    :param clf: classifier to be tested
    :param mfcc_parameters: dictionary with mfcc parameters for tree algorithms
    :param path_results: path to directory containing current processing files
    :param probe: path to test files
    :param path_speaker_models: path to GMM models of speakers
    :param mfcc_parameters_fusion: dictionary with mfcc parameters for GMM algorithm
    """

    test_files = glob.glob(probe + "*.wav")
    test_files.sort()
    print len(test_files)
    start_row = ['']
    model_list = ['f118', 'f122', 'f123', 'f125', 'f126', 'f127', 'f128', 'f129', 'f130', 'f131', 'f133', 'f302',
                  'f303', 'f306', 'f307', 'f309', 'f311', 'f320']

    models_list_gmm = file_utils.get_file_list(path_speaker_models, 'hdf5')

    models_list_gmm.sort()

    models_gmm = {}

    for gmm_file in models_list_gmm:
        gmm_hdf5_file = bob.io.base.HDF5File(gmm_file, 'r')
        models_gmm[gmm_file.split("_")[-1][:4]] = bob.learn.misc.GMMMachine(gmm_hdf5_file)
        del gmm_hdf5_file

    for i in range(0, len(model_list)):
        start_row.append(model_list[i])
    start_row.append('decision')
    result_file_csv = open(path_results + "results.csv", 'wb')
    wr = csv.writer(result_file_csv, dialect='excel')
    wr.writerow(start_row)
    score = 0
    point = 1
    for processing_file in test_files:
        features, models = mfcc_extraction_dt.get_features_file(processing_file, mfcc_parameters, False)

        print "Processing progress " + str(point) + "/" + str(len(test_files))
        processing_file_results = [processing_file]
        results = {}
        results_gmm = {}
        for i in range(0, len(model_list)):
            results[model_list[i]] = 0
            results_gmm[model_list[i]] = 0

        for i in range(0, len(features)):
            key = str(clf.predict(features[i])[0])
            results[key] += 1

        # indeksy results o najwiekszych wartosciach dopasowania
        # result_keys = sorted(results, key=results.get, reverse=True)

        tree_sum = 0
        for key in model_list:
            tree_sum += results[key]

        for key in results.keys():
            # if key not in result_keys:
            # results[key] = 0
            # else:
            #     results[key] = results[key]/tree_sum
            results[key] = results[key] / tree_sum

        features = mfcc_extraction.get_features_file(processing_file, mfcc_parameters_fusion)
        for feature_vector_no in range(0, len(features)):
            results_feat = [0] * 18
            for model_no in range(0, len(model_list)):
                results_feat[model_no] = models_gmm[model_list[model_no]](features[feature_vector_no])
            results_gmm[model_list[results_feat.index(max(results_feat))]] += 1

        # result_keys_gmm = sorted(results_gmm, key=results_gmm.get, reverse=True)

        gmm_sum = 0
        for key in model_list:
            gmm_sum += results_gmm[key]

        for key in results.keys():
            # if key not in model_list:
            # results[key] = 0
            # else:
            #     results[key] += results_gmm[key]/gmm_sum
            results[key] += results_gmm[key] / gmm_sum

        for key in sorted(results):
            processing_file_results.append(results[key])

        result_max = str(max(results, key=results.get))

        if result_max in processing_file:
            processing_file_results.append(1)
            score += 1
        else:
            processing_file_results.append(0)

        wr.writerow(processing_file_results)
        point += 1
    percentage = score / len(test_files) * 100
    wr.writerow([percentage])
    result_file_csv.close()
    percentage_str = str(round(percentage, 3)) + "%"
    os.rename(path_results[:-1], path_results[:-1] + " " + percentage_str)
    return percentage_str


def probe_fusion_addition(clf, mfcc_parameters, path_results, probe, path_speaker_models, mfcc_parameters_fusion):
    """
    Function used to perform classification test for hybrid algorithm when fusion_addition scoring method was chosen
    :param clf: classifier to be tested
    :param mfcc_parameters: dictionary with mfcc parameters for tree algorithms
    :param path_results: path to directory containing current processing files
    :param probe: path to test files
    :param path_speaker_models: path to GMM models of speakers
    :param mfcc_parameters_fusion: dictionary with mfcc parameters for GMM algorithm
    """

    test_files = glob.glob(probe + "*.wav")
    test_files.sort()
    print len(test_files)
    start_row = ['']
    model_list = ['f118', 'f122', 'f123', 'f125', 'f126', 'f127', 'f128', 'f129', 'f130', 'f131', 'f133', 'f302',
                  'f303', 'f306', 'f307', 'f309', 'f311', 'f320']

    models_list_gmm = file_utils.get_file_list(path_speaker_models, 'hdf5')

    models_list_gmm.sort()

    models_gmm = {}

    for gmm_file in models_list_gmm:
        gmm_hdf5_file = bob.io.base.HDF5File(gmm_file, 'r')
        models_gmm[gmm_file.split("_")[1][:4]] = bob.learn.misc.GMMMachine(gmm_hdf5_file)
        del gmm_hdf5_file

    for i in range(0, len(model_list)):
        start_row.append(model_list[i])
    start_row.append('decision')
    result_file_csv = open(path_results + "results.csv", 'wb')
    wr = csv.writer(result_file_csv, dialect='excel')
    wr.writerow(start_row)
    score = 0
    point = 1
    for processing_file in test_files:
        features, models = mfcc_extraction_dt.get_features_file(processing_file, mfcc_parameters, False)

        print "Processing progress " + str(point) + "/" + str(len(test_files))
        processing_file_results = [processing_file]
        results = {}
        for i in range(0, len(model_list)):
            results[model_list[i]] = 0

        for i in range(0, len(features)):
            key = str(clf.predict(features[i])[0])
            results[key] += 1

        # # indeksy results o najwiekszych wartosciach dopasowania
        # result_keys = sorted(results, key=results.get, reverse=True)
        #
        # for key in results.keys():
        # if key not in result_keys:
        #         results[key] = 0

        features = mfcc_extraction.get_features_file(processing_file, mfcc_parameters_fusion)
        for feature_vector_no in range(0, len(features)):
            results_feat = [0] * 18
            for model_no in range(0, len(model_list)):
                results_feat[model_no] = models_gmm[model_list[model_no]](features[feature_vector_no])
            results[model_list[results_feat.index(max(results_feat))]] += 1

        for key in sorted(results):
            processing_file_results.append(results[key])

        result_max = str(max(results, key=results.get))

        if result_max in processing_file:
            processing_file_results.append(1)
            score += 1
        else:
            processing_file_results.append(0)

        wr.writerow(processing_file_results)
        point += 1
    percentage = score / len(test_files) * 100
    wr.writerow([percentage])
    result_file_csv.close()
    percentage_str = str(round(percentage, 3)) + "%"
    os.rename(path_results[:-1], path_results[:-1] + " " + percentage_str)
    return percentage_str
