import csv
import datetime
import os
from math import ceil, floor

from feature_extraction import mfcc_extraction
from gmm_ubm import train_speakers


def float_round(num, places=0, direction=floor):
    return direction(num * (10 ** places)) / float(10 ** places)


path_model_training = '/home/ragnar/speaker_data/enroll/'
path_results = '/home/ragnar/SpeakerRecognition/discretization/' + str(datetime.datetime.now())[0:-7] + '/'

os.makedirs(path_results)

win_length_ms = 30  # The window length of the cepstral analysis in milliseconds
win_shift_ms = 10  # The window shift of the cepstral analysis in milliseconds
n_filters = 80  # The number of filter bands
n_ceps = 46  # The number of cepstral coefficients
f_min = 0.  # The minimal frequency of the filter bank
f_max = 8000.  # The maximal frequency of the filter bank
delta_win = 2  # The integer delta value used for computing the first and second order derivatives
pre_emphasis_coef = 0.97  # The coefficient used for the pre-emphasis
dct_norm = True  # A factor by which the cepstral coefficients are multiplied
mel_scale = True  # Tell whether cepstral features are extracted on a linear (LFCC) or Mel (MFCC) scale
with_delta = False  # If MFCC deltas are computed
with_delta_delta = False  # If MFCC deltas are computed
with_energy = False  # if another energy feature MFCC[0]
vad_type = 0  # if 0 energy vad if 1 stat vad

mfcc_parameters = {'win_length_ms': win_length_ms, 'win_shift_ms': win_shift_ms, 'n_filters': n_filters,
                   'n_ceps': n_ceps, 'f_min': f_min, 'f_max': f_max, 'delta_win': delta_win,
                   'pre_emphasis_coef': pre_emphasis_coef, 'dct_norm': dct_norm, 'mel_scale': mel_scale,
                   'with_delta': with_delta, 'with_delta_delta': with_delta_delta, 'with_energy': with_energy,
                   'vad_type': vad_type}

start = str(datetime.datetime.now())

file_list = train_speakers.get_file_list(path_model_training)

print file_list

resultFile = open(path_results + "results.csv", 'wb')
wr = csv.writer(resultFile, dialect='excel')

total = 0

for i in file_list.keys():
    print "Processing speaker " + i[1:4] + " files"
    features = mfcc_extraction.get_features(file_list[i], mfcc_parameters)
    total += len(features)
    for feat in features:
        result = []
        for f in feat:
            result.append(float_round(f, 6, ceil))
        result.append(i[1:4])
        wr.writerow(result)

resultFile.close()
print "*****************************************"
print "Start time: " + start
print "End time: " + str(datetime.datetime.now())


