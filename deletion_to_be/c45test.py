__author__ = 'ragnar'

import glob

import Orange.classification.tree as orange
import Orange

from feature_extraction import mfcc_extraction_dt


win_length_ms = 30  # The window length of the cepstral analysis in milliseconds
win_shift_ms = 10  # The window shift of the cepstral analysis in milliseconds
n_filters = 44  # The number of filter bands
n_ceps = 28  # The number of cepstral coefficients
f_min = 0.  # The minimal frequency of the filter bank
f_max = 8000.  # The maximal frequency of the filter bank
delta_win = 2  # The integer delta value used for computing the first and second order derivatives
pre_emphasis_coef = 0.97  # The coefficient used for the pre-emphasis
dct_norm = True  # A factor by which the cepstral coefficients are multiplied
mel_scale = True  # Tell whether cepstral features are extracted on a linear (LFCC) or Mel (MFCC) scale
with_delta = True  # If MFCC deltas are computed
with_delta_delta = True  # If MFCC deltas are computed
with_energy = True  # if another energy feature MFCC[0]
vad_type = 0  # if 0 energy vad if 1 stat vad

training = '/home/ragnar/speaker_data/enroll/'

mfcc_parameters = {'win_length_ms': win_length_ms, 'win_shift_ms': win_shift_ms, 'n_filters': n_filters,
                   'n_ceps': n_ceps, 'f_min': f_min, 'f_max': f_max, 'delta_win': delta_win,
                   'pre_emphasis_coef': pre_emphasis_coef, 'dct_norm': dct_norm, 'mel_scale': mel_scale,
                   'with_delta': with_delta, 'with_delta_delta': with_delta_delta, 'with_energy': with_energy,
                   'vad_type': vad_type}

file_list = glob.glob(training + "*.wav")
features = mfcc_extraction_dt.get_features(file_list, mfcc_parameters, True)

# print len(features)


# print np.concatenate(features, models)

feat = [Orange.feature.Continuous('a%i' % x) for x in range(len(features[1]) - 1)]
clazz = ['f118', 'f122', 'f123', 'f125', 'f126', 'f127', 'f128', 'f129', 'f130', 'f131', 'f133', 'f302',
         'f303', 'f306', 'f307', 'f309', 'f311', 'f320']
class_ = [Orange.feature.Discrete('class', values=clazz)]

domain = Orange.data.Domain(feat + class_, clazz)

print domain.class_var
#
data = Orange.data.Table(domain, features)

import datetime

print datetime.datetime.now()

tree = orange.C45Learner(data)
print datetime.datetime.now()

