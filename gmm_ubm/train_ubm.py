__author__ = 'Maciej Aleszczyk'

import datetime

import bob.io.base
import bob.learn.misc

from feature_extraction import mfcc_extraction
from misc import file_utils


def prepare_features(path_ubm_training, mfcc_parameters):
    """
    Function prepares data to traing UBM model features
    :param path_ubm_training: part where wav files to train UBM model are stored
    :param mfcc_parameters: parameters of MFCC processing
    :return: :rtype: path to UBM features file
    """
    print "Creating MFCC features"
    now = datetime.datetime.now()
    print(str(now))
    ubm_train_files = file_utils.get_file_list(path_ubm_training, 'wav')
    features = mfcc_extraction.get_features(ubm_train_files, mfcc_parameters)

    # mfcc_extraction.save_features(path_results + 'ubm_features.hdf5', features)
    # del features
    # return path_results + 'ubm_features.hdf5'
    return features


def gmm_ubm_train(path_results, features, gmm_parameters):
    """
    Function trains UBM model
    :param path_results: path where trained model will be saved
    :param features: path to hdf5 file with MFCC features
    :param gmm_parameters: GMM processing parameters
    :return: :rtype: path to hdf5 file with trained UBM model
    """
    # print "Opening MFCC features"
    # features = mfcc_extraction.open_features_file(ubm_feature_path)

    print "Process! kmeans"
    now = datetime.datetime.now()
    print(str(now))
    kmeans = bob.learn.misc.KMeansMachine(gmm_parameters['gaussians_no'], len(features[0]))
    kmeansTrainer = bob.learn.misc.KMeansTrainer()
    kmeansTrainer.max_iterations = gmm_parameters['max_iterations']
    kmeansTrainer.convergence_threshold = gmm_parameters['convergence_threshold']
    kmeansTrainer.train(kmeans, features)

    [variances, weights] = kmeans.get_variances_and_weights_for_each_cluster(features)

    print "-----------------------------------"
    print "Process! gmm"
    now = datetime.datetime.now()
    print(str(now))
    ubm = bob.learn.misc.GMMMachine(gmm_parameters['gaussians_no'],
                                    len(features[0]))
    ubm.means = kmeans.means
    ubm.variances = variances

    trainer = bob.learn.misc.ML_GMMTrainer(update_means=True, update_variances=True, update_weights=True)
    trainer.convergence_threshold = gmm_parameters['convergence_threshold']
    trainer.max_iterations = gmm_parameters['max_iterations']
    trainer.train(ubm, features)

    print "-----------------------------------"
    print "Saving to GMM-UBM File"
    now = datetime.datetime.now()
    print(str(now))
    ubm_file = bob.io.base.HDF5File(path_results + '/ubm_model.hdf5', 'w')
    ubm.save(ubm_file)
    del ubm_file
    # file_utils.save_gmm(path_results + '/ubm_model.hdf5',ubm)
    return path_results + '/ubm_model.hdf5'


def run(path_results, path_ubm_training, mfcc_parameters, gmm_parameters):
    """
    Function which is used to train UBM model
    :param path_results: path where model will be saved
    :param path_ubm_training: path to training data
    :param mfcc_parameters: mfcc processing parameters
    :param gmm_parameters: gmm processing parameters
    :return: :rtype:
    """
    features = prepare_features(path_ubm_training, mfcc_parameters)
    path_ubm_model = gmm_ubm_train(path_results, features, gmm_parameters)
    return path_ubm_model