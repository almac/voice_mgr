__author__ = 'Maciej Aleszczyk'

import time

from misc import file_utils
from feature_extraction import mfcc_extraction


def run(path_results, path_probe_data, mfcc_parameters):
    """
    Function prepares data for test. It creates processed directory in input path and put hdf5 files with MFCC parameters
    :param path_probe_data: path to test files
    :param path_results: path to directory containing current processing files
    :param mfcc_parameters: dictionary with MFCC parameters
    """
    file_list = file_utils.get_file_list(path_probe_data, 'wav')

    start = time.time()

    for wav_file in file_list:
        file_name = wav_file.split('/')[-1][:-4]
        print "      ************       "
        print "Processing " + file_name
        print "      ************       "

        features = mfcc_extraction.get_features_file(wav_file, mfcc_parameters)
        mfcc_extraction.save_features(path_results + 'probe/' + file_name + '.hdf5', features)

    stop = time.time()

    print "Processing time: " + str(stop - start) + " s"
    return path_results + 'probe/'
