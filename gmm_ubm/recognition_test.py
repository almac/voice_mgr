from __future__ import division

__author__ = 'Maciej Aleszczyk'

from misc import file_utils
import os
import csv
import bob.io.base
import bob.learn.misc


def run(path_results, path_speaker_models, path_test_data):
    """
    Main function of testing recognition abilities of speaker models.
    :param path_results: path to creating file with results
    :param path_speaker_models: path to speaker models
    :param path_test_data: path to test data
    """
    models_list = file_utils.get_file_list(path_speaker_models, 'hdf5')
    files_list = file_utils.get_file_list(path_test_data, 'hdf5')

    files_list.sort()
    models_list.sort()

    models = []

    for gmm_file in models_list:
        gmm_hdf5_file = bob.io.base.HDF5File(gmm_file, 'r')
        models.append(bob.learn.misc.GMMMachine(gmm_hdf5_file))
        del gmm_hdf5_file

    i = 1
    j = 0

    resultFile = open(path_results + "results.csv", 'wb')
    wr = csv.writer(resultFile, dialect='excel')
    start_row = ['']

    for model in models_list:
        start_row.append(model.split("_")[-1][0:-5])
    start_row.append('decision')
    wr.writerow(start_row)

    for feature_file in files_list:
        features = file_utils.open_features_file(feature_file)

        print "Processing progress " + str(i) + "/" + str(len(files_list))
        results_file = []
        results_feat = [0] * len(models_list)
        results = [0] * len(models_list)
        results_file.append(feature_file.split('/')[-1])
        # results_file.append(feature_file)
        # for model_no in range(0, len(models_list)):
        # loglikelihood = 0
        #
        # for feature_vector_no in range(0, len(features)):
        # loglikelihood += models[model_no](features[feature_vector_no])
        #     results_file.append(loglikelihood)

        for feature_vector_no in range(0, len(features)):
            for model_no in range(0, len(models_list)):
                results_feat[model_no] = models[model_no](features[feature_vector_no])
            results[results_feat.index(max(results_feat))] += 1

        '''
        Finds model name connected with max LogLikelihood

        max(results_file[1:]) - search max value in loglikelihood results for processed file
        results_file.index(...) - gets index of max value of LogLikelihood for processed file
        models_list[].split("_")[1][0:-5] - gets name of model
        '''
        results_file.extend(results)
        model_name = models_list[results_file[1:].index(max(results_file[1:]))].split("_")[-1][0:-5]
        results_file.append(model_name)

        if model_name in feature_file:
            results_file.append(1)
            j += 1
        else:
            results_file.append(0)

        wr.writerow(results_file)

        i += 1

    percentage = j / len(files_list) * 100
    wr.writerow([percentage])
    resultFile.close()
    percentage_str = str(round(percentage, 3)) + "%"
    os.rename(path_results[:-1], path_results[:-1] + " " + percentage_str)
    return percentage_str
