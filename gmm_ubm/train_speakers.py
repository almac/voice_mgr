__author__ = 'ragnar'

import glob
import time

import bob.learn.misc

from feature_extraction import mfcc_extraction
from misc import file_utils


def get_file_list(path):
    """
    Function creates a dictionary with speaker code as key and sublist of enroll speech files as values
    :param path: path to enrollment set
    :return: dictionary of speaker and enrollment set speech files tied to speaker
    """
    file_list = glob.glob(path + "*.wav")
    splitted_file_list = {}
    for i in file_list:
        new_item = i.split('/enroll/')[1].split('_')
        if new_item[0] in splitted_file_list:
            splitted_file_list[new_item[0]].append(i)
        else:
            splitted_file_list[new_item[0]] = []
            splitted_file_list[new_item[0]].append(i)
    return splitted_file_list


def prepare_adaptating_data(path_results, path_model_training, mfcc_parameters):
    """
    Function computes MFCC features to be used in MAP-GMM model creation for speaker. It combines MFCC features from
    different wav files specific for one speaker from enrollment dataset. Produced files are put in processed dir in
    input path.
    :param path_results: result path, there will be created adaptated models training data
    :param path_model_training: path to path to training files to obtain MFCC parameters to teach speaker models from UBM
    :param mfcc_parameters:
    """
    file_list = get_file_list(path_model_training)

    for i in file_list.keys():
        print
        print "           *******             "
        print "Processing speaker " + i + " files"
        print "           *******             "
        print
        features = mfcc_extraction.get_features(file_list[i], mfcc_parameters)
        mfcc_extraction.save_features(path_results + '/models/features/' + i + '.hdf5', features)

    return path_results + 'models/features/'


def run(path_results, path_ubm_model, path_model_training, mfcc_parameters, gmm_parameters):
    """
    Function uses MAP-GMM to create model for each speaker. Created models are saved in adapted directory in input path.
    :param path_results: path where speaker models will be created
    :param path_ubm_model: path to UBM model
    :param path_model_training: path to hdf5 files with training data
    :param mfcc_parameters: parameters of MFCC processing
    :param gmm_parameters: parameters of GMM processing
    """

    model_features = prepare_adaptating_data(path_results, path_model_training, mfcc_parameters)

    file_list = file_utils.get_file_list(model_features, 'hdf5')
    ubm_file = bob.io.base.HDF5File(path_ubm_model, 'r')
    ubm = bob.learn.misc.GMMMachine(ubm_file)
    del ubm_file

    relevance_factor = gmm_parameters['relevance_factor']
    gmm_map_trainer = bob.learn.misc.MAP_GMMTrainer(relevance_factor, True, False, False)  # mean adaptation only
    gmm_map_trainer.convergence_threshold = gmm_parameters['convergence_threshold']
    gmm_map_trainer.max_iterations = gmm_parameters['max_iterations']
    gmm_map_trainer.set_prior_gmm(ubm)

    start = time.time()

    for i in range(0, len(file_list)):
        print
        print "             *******             "
        print "Processing speaker " + file_list[i][-9:-5] + " MAP-GMM adaptation"
        print "             *******             "
        print
        features = file_utils.open_features_file(file_list[i])
        print len(features[0])
        print ubm.dim_c
        print ubm.dim_d

        gmm_adapted = bob.learn.misc.GMMMachine(ubm.dim_c, ubm.dim_d)
        gmm_map_trainer.train(gmm_adapted, features)
        gmm_file = bob.io.base.HDF5File(path_results + 'models/model_' + file_list[i][-9:-5] + '.hdf5', 'w')
        gmm_adapted.save(gmm_file)
        del gmm_file

    stop = time.time()
    print "total time " + str(stop - start)
    return path_results + 'models/'


def run_gmm(path_results, path_model_training, mfcc_parameters,
            gmm_parameters):
    """
    Function uses GMM to create model for each speaker. Created models are saved in adapted directory in input path.
    :param path_results: path where speaker models will be created
    :param path_model_training: path to hdf5 files with training data
    :param mfcc_parameters: parameters of MFCC processing
    :param gmm_parameters: parameters of GMM processing
    """
    model_features = prepare_adaptating_data(path_results, path_model_training, mfcc_parameters)

    file_list = file_utils.get_file_list(model_features, 'hdf5')

    features_len = len(file_utils.open_features_file(file_list[0])[0])

    kmeans = bob.learn.misc.KMeansMachine(gmm_parameters['gaussians_no'], features_len)
    kmeans_trainer = bob.learn.misc.KMeansTrainer()
    kmeans_trainer.max_iterations = gmm_parameters['max_iterations']
    kmeans_trainer.convergence_threshold = gmm_parameters['convergence_threshold']

    gmm_trainer = bob.learn.misc.ML_GMMTrainer(update_means=True, update_variances=True, update_weights=True)
    gmm_trainer.convergence_threshold = gmm_parameters['convergence_threshold']
    gmm_trainer.max_iterations = gmm_parameters['max_iterations']

    for i in range(0, len(file_list)):
        print
        print "             *******             "
        print "Processing speaker " + file_list[i][-9:-5] + " GMM model creation"
        print "             *******             "
        print
        features = file_utils.open_features_file(file_list[i])

        print "Process! kmeans"
        kmeans_trainer.train(kmeans, features)
        gmm = bob.learn.misc.GMMMachine(gmm_parameters['gaussians_no'], features_len)
        gmm.means = kmeans.means

        print "Process! gmm"
        gmm_trainer.train(gmm, features)

        gmm_file = bob.io.base.HDF5File(path_results + 'models/model_' + file_list[i][-9:-5] + '.hdf5', 'w')
        gmm.save(gmm_file)
        del gmm_file

    return path_results + 'models/'