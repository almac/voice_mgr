__author__ = 'Maciej Aleszczyk'

'''
Features per speaker:

input: list of files with samples of speech specific for each speaker (enroll set)
output: hdf5 files, each containing MFCC parameters specific for speaker

Enroll set path: /home/ragnar/speaker_data/enroll/
'''

import glob

from feature_extraction import mfcc_extraction


def get_file_list(path):
    """
    Function creates a dictionary with speaker code as key and sublist of enroll speech files as values
    :param path: path to enrollment set
    :return: :rtype: dictionary of speaker and enrollment set speech files tied to speaker
    """
    file_list = glob.glob(path + "*.wav")
    splitted_file_list = {}
    for i in file_list:
        new_item = i.split('/enroll/')[1].split('_')
        if new_item[0] in splitted_file_list:
            splitted_file_list[new_item[0]].append(i)
        else:
            splitted_file_list[new_item[0]] = []
            splitted_file_list[new_item[0]].append(i)
    return splitted_file_list


def run(path):
    """
    Function computes MFCC features to be used in MAP-GMM model creation for speaker. It combines MFCC features from
    different wav files specific for one speaker from enrollment dataset. Produced files are put in processed dir in
    input path.
    :param path: path to enrollment set with wav files
    """
    file_list = get_file_list(path)

    for i in file_list.keys():
        print
        print "           *******             "
        print "Processing speaker " + i + " files"
        print "           *******             "
        print
        features = mfcc_extraction.get_features(file_list[i], True)
        mfcc_extraction.save_features(path + 'processed/' + i + '.hdf5', features)


        # run('/home/ragnar/speaker_data/enroll/')

        # to be added later
        # if len(sys.argv) == 2:
        # run(sys.argv[1])
        # else:
        # print "Not enough parameters. Please run again adding path."